class Event: #generate a class Event which contains the timestamp and the type of Event
   def __init__(self, type, timestamp, servicetime, is_dropped = False):
      self.type = type
      self.timestamp = timestamp
      self.servicetime = servicetime #only for arrivals, for observer and departure this is irellevant.
      self.is_dropped = is_dropped # flag to indicate whether the particular event has been dropped for a finite queue