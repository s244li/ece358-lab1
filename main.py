from Event import Event
import random 
import math
import statistics
import matplotlib.pyplot as plt

T = 1000 # Time in seconds
L = 2000 # Average length of a packet in bits
C = 1000000 # The transmission rate of the output link in bits per second

t_arrival = []

t_depart = []

t_observer = []

d = 0 #departcounter
a = 0 #arrivecounter
o = 0 #observercounter
s = 0 #servicecnt

def generate_list_of_exponential_random_variables(lam):
    list_of_exponential_random_variables = []
    i = 0
    while i < 1000:
        list_of_exponential_random_variables.append(generate_exponential_random_variable(lam))
        i+=1
    return list_of_exponential_random_variables

def generate_exponential_random_variable(lam): #time to arrive
    return -(1/lam)*math.log(1 -(random.uniform(0,1)))

def generate_arrival_events(lam, eventlist):
    arrivaltime = 0
    while arrivaltime < T:
        arrivaltime += generate_exponential_random_variable(lam)
        servicetime = generate_exponential_random_variable(1/L) / C #length of packet/1Mbitps
        arrivalinfo = Event("arrival",arrivaltime, servicetime)
        t_arrival.append(arrivalinfo)
        eventlist.append(arrivalinfo)

def generate_departure_events(eventlist):
    departuretime = 0
    for event in eventlist:
        if event.type == "arrival":
            if event.timestamp > departuretime:
                departuretime = event.timestamp + event.servicetime
            else:
                departuretime += event.servicetime
            departinfo = Event("departure", departuretime, 0)
            eventlist.append(departinfo)

# used in the case of a finite buffer size and checking if a packet should be dropped
def intervene_and_generate_departure_events_for_finite_queue(eventlist, k):
    departuretime = 0
    parallel_list = list(eventlist)
    next(iter(parallel_list))
    for event, nextevent in zip(eventlist, parallel_list):
        if event.type == "arrival" and not event.is_dropped:
            if event.timestamp > departuretime:
                departuretime = event.timestamp + event.servicetime
            else:
                departuretime += event.servicetime
            
            size_of_queue = 0
            # essentially we want to check if packet event has come or arrived between another packet that is being processed and see if it can fit in the buffer
            if nextevent.type == "arrival" and not nextevent.is_dropped and nextevent.timestamp < departuretime and nextevent.timestamp >= event.timestamp:
                if size_of_queue >= k:
                    pkt.is_dropped = True
                else:
                    size_of_queue += 1
            departinfo = Event("departure", departuretime, 0)
            eventlist.append(departinfo)

def generate_observer_events(rate, eventlist):
    global o
    observertime = 0
    while observertime < T:
        observertime += generate_exponential_random_variable(rate)
        observerinfo = Event("observer",observertime, 0)
        t_observer.append(observerinfo)
        eventlist.append(observerinfo)

def perform_observation(eventlist, finite_buffer_size = None):
    if finite_buffer_size is not None:
        return perform_observation_with_buffer_size(eventlist, finite_buffer_size)

    num_packet_arrival = 0
    num_packet_departures = 0
    num_packet_observations = 0
    num_packets_in_queue = 0
    total_packets_counter = 0
    idle = 0

    for event in eventlist:
        if event.type == "arrival":
            num_packet_arrival += 1
        elif event.type == "departure":
            num_packet_departures += 1
        elif event.type == "observer":
            num_packet_observations += 1
            packets_in_buffer = num_packet_arrival - num_packet_departures
            if (packets_in_buffer == 0):
                idle += 1
            total_packets_counter += packets_in_buffer
    
    p_idle = idle / num_packet_observations
    average_packets_in_queue = total_packets_counter / num_packet_observations
    return p_idle, average_packets_in_queue

# variation of perform_observation particularly to record probability of packet loss
def perform_observation_with_buffer_size(eventlist, k):
    num_packet_arrival = 0
    num_packet_departures = 0
    num_packet_observations = 0
    num_packets_in_queue = 0
    total_packets_counter = 0
    num_packet_dropped = 0

    for event in eventlist:
        if event.type == "arrival":
            if event.is_dropped:
                num_packet_dropped += 1
            else:
                num_packet_arrival += 1
        elif event.type == "departure":
            num_packet_departures += 1
        elif event.type == "observer":
            num_packet_observations += 1
            packets_in_buffer = num_packet_arrival - num_packet_departures
            total_packets_counter += packets_in_buffer

    average_packets_in_queue = total_packets_counter / num_packet_observations
    p_loss = num_packet_dropped / num_packet_observations
    return average_packets_in_queue, p_loss
                
def run_simulation(lam, finite_buffer_size = None):
    eventlist = []
    generate_arrival_events(lam, eventlist)
    if finite_buffer_size is not None:
        intervene_and_generate_departure_events_for_finite_queue(eventlist, finite_buffer_size)
    else:
        generate_departure_events(eventlist)
    generate_observer_events(5 * lam, eventlist) # generate events at rate 5 times lambda
    eventlist.sort(key=lambda event:event.timestamp)
    return perform_observation(eventlist, finite_buffer_size)

def build_graph(rho_values, ylist, ylabel, png_name):
    plt.plot(rho_values, ylist)
    plt.xlabel("Traffic intensity \u03C1")
    plt.ylabel(ylabel)
    plt.savefig(png_name, bbox_inches="tight")
    plt.close()

def build_graph_for_finite_queue(rho_values, k_values, ylists, ylabel, png_name):
    plt.xlabel("Traffic intensity \u03C1")
    plt.ylabel(ylabel)
    plt.plot(rho_values, ylists[0], label=f'k = {k_values[0]}')
    plt.plot(rho_values, ylists[1], label=f'k = {k_values[1]}')
    plt.plot(rho_values, ylists[2], label=f'k = {k_values[2]}')
    plt.legend(loc="upper left")
    plt.savefig(png_name, bbox_inches="tight")
    plt.close()

def question_1():
    lambda_rate = 75
    list_of_exponential_random_variables = generate_list_of_exponential_random_variables(lambda_rate)
    mean = statistics.mean(list_of_exponential_random_variables)
    variance = statistics.variance(list_of_exponential_random_variables)
    print("Exponential random variable mean is %s and the variance is %s" % (str(mean), str(variance)))

def question_3():
    rho_list_of_values = [0.35, 0.45, 0.55, 0.65, 0.75, 0.85]
    p_idle_list = []
    average_num_pkts_list = []
    for rho in rho_list_of_values:
        lambda_arrival_rate = (rho * C) / L 
        p_idle, average_num_pkts = run_simulation(lambda_arrival_rate)
        p_idle_list.append(p_idle)
        average_num_pkts_list.append(average_num_pkts)
    build_graph(rho_list_of_values, average_num_pkts_list, "Average number of packets in system E[N]", "q3-1")
    build_graph(rho_list_of_values, p_idle_list, "Proportion of time system idle PIDLE", "q3-2")
        
def question_6():
    rho_list_of_values = [0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4]
    k_list_of_values = [10, 25, 50]
    p_loss_list = []
    average_num_pkts_list = []
    for k in k_list_of_values:
        p_loss_list_for_k = []
        average_num_pkts_list_for_k = []
        for rho in rho_list_of_values:
            lambda_arrival_rate = (rho * C) / L 
            average_num_pkts, p_loss = run_simulation(lambda_arrival_rate, k)
            p_loss_list_for_k.append(p_loss)
            average_num_pkts_list_for_k.append(average_num_pkts)
        p_loss_list.append(p_loss_list_for_k)
        average_num_pkts_list.append(average_num_pkts_list_for_k)
    build_graph_for_finite_queue(rho_list_of_values, k_list_of_values, average_num_pkts_list, "Average number of packets in system E[N]", "q6-1")
    build_graph_for_finite_queue(rho_list_of_values, k_list_of_values, p_loss_list, "Packet loss probability PLOSS", "q6-2")

def main():
    print("QUESTION 1")
    question_1()
    print("QUESTION 3")
    question_3()
    print("QUESTION 6")
    question_6()
    print('done')

if __name__ == "__main__":
    main()   
